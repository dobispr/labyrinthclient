# LabyrinthClient
The game client for Labyrinth. Uses the Unity Game Engine and Socket.io to communicate with the server. Written in C#.

A built out binary is available in the Downloads section.