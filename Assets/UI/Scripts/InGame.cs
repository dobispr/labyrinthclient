﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class InGame : MonoBehaviour {

	public Text GameInfo;

	public Button ConfirmAction;
	public GameObject EndTurn;

	public GameObject jumpButtonYouPrefab;
	public GameObject jumpButtonThemPrefab;

	[System.NonSerialized]
	public MapObject selectedUnit;

	public Selector selector;

	public InfoPanelSystem infoPanel;
	public GameObject actionButtons;

	private MatchManager match;

	private bool alreadyDismissed;

	public Unit tempSelectedUnit;

	private Canvas canvas;

	private int numDead;
	private int numEnemy;
	private List<GameObject> jumpButtons;

	// Use this for initialization
	void Start () {
		match = MatchManager.instance;
//		infoPanel.gameObject.SetActive(false);
		alreadyDismissed = false;
		tempSelectedUnit = selector.SelectedUnit;
		canvas = gameObject.GetComponent<Canvas>();
		numDead = 0;
		numEnemy = 0;
		jumpButtons = new List<GameObject>();
		if (match) displayJumpButtons();
	}
	
	// Update is called once per frame
	void Update () {
		if (!tempSelectedUnit) tempSelectedUnit = selector.SelectedUnit;
		DebugHUD.setValue("Is dismissed", infoPanel.dismissButton.Dismissed);
		if (match) {
			updateGameInfo();
//			if (selector.SelectedUnit) {
//				if (!selector.SelectedUnit.Equals(selectedUnit) || infoPanel.dismissButton.Dismissed) {
					//selectedUnit = selector.SelectedUnit;
					infoPanel.updateSelectedInfo(selectedUnit);
//					infoPanel.gameObject.SetActive(true);
//					infoPanel.dismissButton.Dismissed = false;
//					alreadyDismissed = false;
//				}
//			} else {
////				infoPanel.gameObject.SetActive(false);
//				if (!alreadyDismissed) {
//					infoPanel.dismissButton.Dismissed = true;
//					alreadyDismissed = true;
//				}
//			}
			EndTurn.SetActive(match.MyTurn);
			if (checkIfButtonsNeedsRefreshing()) displayJumpButtons();
		} else {
			match = MatchManager.instance;
			displayJumpButtons();
		}
	}

	// Check to see if the number of dead units and the number of enemies we can see is the same as what we thought
	// If they're not, we need to redo the jump buttons
	// This way we don't have to insantiate them every frame
	public bool checkIfButtonsNeedsRefreshing() {
		int tempNumDead = 0;
		int tempNumEnemy = 0;
		foreach (Unit unit in match.MapObjects.Values.Where(x => x is Unit)) {
			if (unit.IsDead) {
				tempNumDead++;
			} else if (unit.ownerID != GameManager.instance.Username && unit.Tile.VisionState.Equals(VisionState.VISIBLE)) {
				tempNumEnemy++;
			}
		}
		return tempNumDead != numDead || tempNumEnemy != numEnemy;
	}
	public void displayJumpButtons() {
		Debug.Log("Drawing Jump Buttons");
		int count = 0;
		int youCount = 0;
		int currentOffsetCount = 0;
		int monsterCount = 0;
		ClearButtons();
		foreach (Unit unit in match.MapObjects.Values.Where(x => x is Unit)) {
			if (unit.IsDead) {
				numDead++;
				continue;
			}
			GameObject jumpButton;
			if (unit.ownerID == GameManager.instance.Username) {
				jumpButton = Instantiate(jumpButtonYouPrefab);
				currentOffsetCount = youCount++;
			} else {
				bool isVisible = unit.Tile.VisionState.Equals(VisionState.VISIBLE);
				if (isVisible) {
					jumpButton = Instantiate(jumpButtonThemPrefab);
					currentOffsetCount = numEnemy++;
				} else {
					continue;
				}
			}
			jumpButtons.Add(jumpButton);
			jumpButton.transform.SetParent(canvas.transform);
			JumpButton jb = jumpButton.GetComponent<JumpButton>();
			RectTransform rt = (RectTransform)jumpButton.transform;
			jb.selector = selector;
			jb.unit = unit;
			float height = jb.anchorMax.y - jb.anchorMin.y;
			rt.anchorMin = new Vector2(jb.anchorMin.x, jb.anchorMin.y - (height + jb.offset) * currentOffsetCount);
			rt.anchorMax = new Vector2(jb.anchorMax.x, jb.anchorMax.y - (height + jb.offset) * currentOffsetCount);
			rt.offsetMin = Vector2.zero;
			rt.offsetMax = Vector2.zero;
			count++;
			string title;
			switch(unit.MOName.ToLower()) {
			case "warrior":
				title = "W";
				break;
			case "mage":
				title = "M";
				break;
			case "rogue":
				title = "R";
				break;
			case "warriorrogue":
				title = "W/R";
				break;
			case "warriormage":
				title = "W/M";
				break;
			case "roguemage":
				title = "R/M";
				break;
			default:
				monsterCount++;
				title = "M" + monsterCount;
				break;
			}
			jb.title.text = title;
		}

	}

	public void ClearButtons()
	{
		numDead = 0;
		numEnemy = 0;
		foreach (GameObject b in jumpButtons)
		{
			removeContentFromDisplay(b);
		}
		jumpButtons.Clear();
	}

	private void removeContentFromDisplay(GameObject b)
	{
		Destroy(b);
	}

	private void updateGameInfo() {
		string opponent = match.OpponentName;
		switch (match.OpponentType) {
		case PlayerType.Architect:
			opponent = "Architect " + opponent;
			break;
		case PlayerType.Heroes:
			opponent = opponent + "'s heroes";
			break;
		default:
			break;
		}
		int turn = match.TurnNumber;
		bool myTurn = match.MyTurn;

		GameInfo.text = "vs " + opponent + " // turn #" + turn + " // " + (myTurn ? "your turn" : "their turn");
	}
}
